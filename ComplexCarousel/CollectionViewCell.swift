//
//  CollectionViewCell.swift
//  ComplexCarousel
//
//  Created by  Masoud Moharrami on 1/1/18.
//  Copyright © 2018  Masoud Moharrami. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var shadowLayer: UIView!
    
    var numberOfItems:Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.delegate = self
        tableView.dataSource = self
        
        self.layer.masksToBounds = false
        
        cellView.layer.cornerRadius = 10
        cellView.layer.masksToBounds = true
        
        shadowLayer.layer.masksToBounds = false
        shadowLayer.layer.cornerRadius = 10
        shadowLayer.layer.shadowRadius = 12
        shadowLayer.layer.shadowOpacity = 0.7
        shadowLayer.layer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.layer.shadowOffset = CGSize(width: 0, height: 8)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("numberOfItems2: \(String(numberOfItems))")
        return numberOfItems
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell
        cell?.configure(pageName: "Page number \(indexPath.row)")
        let pageView: UIView = (cell?.pageView!)!
        pageView.layer.cornerRadius = 8
        pageView.layer.shadowRadius = 4
        pageView.layer.shadowOpacity = 0.4
        pageView.layer.shadowColor = UIColor.darkGray.cgColor
        pageView.layer.shadowOffset = CGSize(width: 0, height: 2)
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func configure(items:Int){
        numberOfItems = items
        print("numberOfItems: \(String(numberOfItems))")
    }
}


