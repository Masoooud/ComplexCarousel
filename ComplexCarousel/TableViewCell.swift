//
//  TableViewCell.swift
//  ComplexCarousel
//
//  Created by  Masoud Moharrami on 1/1/18.
//  Copyright © 2018  Masoud Moharrami. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var pageLabel: UILabel!
    @IBOutlet weak var pageView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(pageName label: String){
        pageLabel.text = label
    }

}
