//
//  Data.swift
//  ComplexCarousel
//
//  Created by  Masoud Moharrami on 1/1/18.
//  Copyright © 2018  Masoud Moharrami. All rights reserved.
//

import Foundation

class Data{
    var store: String!
    var building: String!
    
    init(store: String, building: String) {
        self.store = store
        self.building = building
    }
}
