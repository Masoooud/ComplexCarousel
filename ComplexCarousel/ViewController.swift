//
//  ViewController.swift
//  ComplexCarousel
//
//  Created by  Masoud Moharrami on 1/1/18.
//  Copyright © 2018  Masoud Moharrami. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var carouselView: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    
    var insertedItems: [Int] = []
    var indexes: [Int]!
    var carouselIndex: IndexPath!
   
    let collectionMargin = CGFloat(50)
    let itemSpacing = CGFloat(20)
    var itemHeight: CGFloat!
    var itemWidth = CGFloat(0)
    var currentItem = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        carouselView.delegate = self
        carouselView.dataSource = self
        
        itemHeight = carouselView.bounds.height
        
        setup()

        insertedItems.append(1)
        insertedItems.append(2)
        insertedItems.append(3)
        insertedItems.append(4)
        insertedItems.append(5)
        
        carouselView.reloadData()
//        insertedItems.append(5)
        // Do any additional setup after loading the view, typically from a nib.
    }
    func setup() {
    
        carouselView.showsHorizontalScrollIndicator = false
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        carouselView.allowsSelection = false

        itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        
//        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        carouselView!.collectionViewLayout = layout
        carouselView?.decelerationRate = UIScrollViewDecelerationRateFast
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageController.numberOfPages = insertedItems.count
        return insertedItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectioViewCell", for: indexPath) as? CollectionViewCell
        cell?.configure(items: insertedItems[indexPath.row])
        cell?.tableView.reloadData()
        
        return cell!
    }
    
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let pageWidth = Float(itemWidth + itemSpacing)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(carouselView!.contentSize.width  )
        var newPage = Float(self.pageController.currentPage)
        
        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        } else {
            newPage = Float(velocity.x > 0 ? self.pageController.currentPage + 1 : self.pageController.currentPage - 1)
            if newPage < 0 {
                newPage = 0
            }
            if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        self.pageController.currentPage = Int(newPage)
        
        // PageController.currentPage give us the index of carousel
        
        print("Carousel Index: \(pageController.currentPage)")
        
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
//
        var visibleRect = CGRect()
//
        visibleRect.origin = carouselView.contentOffset
        visibleRect.size = carouselView.bounds.size
        
        self.pageController.currentPage = Int(newPage)

//        let index = pageController.currentPage
//
//        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
//                let visibleIndexPathes: [IndexPath] = collectionView.indexPathsForVisibleItems
//        //
//        //        for index in visibleIndexPathes{
//        //            print("index path row: \(index.row)")
//        //        }
//        var visibleIndexPath: IndexPath!
//
//        if collectionView.indexPathForItem(at: visiblePoint) != nil{
//            visibleIndexPath = collectionView.indexPathForItem(at: visiblePoint)!
//            print(visibleIndexPath.row)
//        }

    }

    
}

